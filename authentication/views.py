from django.contrib.auth import authenticate, login, logout
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, ListAPIView, GenericAPIView
from rest_framework.response import Response
from django.http import HttpResponseBadRequest
from django.contrib.auth.models import User
from authentication.serializers import RegisterSerializer, LoginSerializer


class LoginView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        s = LoginSerializer(data=request.data)
        if s.is_valid(raise_exception=True):
            user = authenticate(
                request, 
                username=s.validated_data['username'], 
                password=s.validated_data['password'])

        if user is not None:
            login(request, user)
            return Response(RegisterSerializer(instance=user).data)
        return HttpResponseBadRequest(content='Wrong username or password!')


class LogoutView(APIView):

    def post(self, request):
        logout(request)
        return Response()
        

class RegisterView(CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = RegisterSerializer


class UserListView(ListAPIView):
    permissions_classes = (permissions.IsAdminUser,)
    serializer_class = RegisterSerializer
    queryset = User.objects.all()