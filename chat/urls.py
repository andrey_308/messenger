from rest_framework.routers import DefaultRouter
from chat.views import ChatView 

router = DefaultRouter()
router.register(r'', ChatView, basename='chat')

urlpatterns = router.urls
