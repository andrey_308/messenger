from rest_framework import viewsets
from chat.serializers import ChatSerializer
from chat.models import Chat


class ChatView(viewsets.ModelViewSet):
    serializer_class = ChatSerializer
    queryset = Chat.objects.all()