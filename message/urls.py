from rest_framework.routers import DefaultRouter
from message.views import MessageView 

router = DefaultRouter()
router.register(r'', MessageView, basename='message')

urlpatterns = router.urls
