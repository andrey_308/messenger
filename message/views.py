from rest_framework import viewsets
from message.serializers import MessageSerializer
from message.models import Message


class MessageView(viewsets.ModelViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
